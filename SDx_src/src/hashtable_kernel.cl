#define PRIME 1900813
#define SUBTABLES  3
#define MAX_ATTEMPTS 5
#define LOCAL_TABLE_SIZE 7371
#define INPUT_SIZE 8192
#define BUCKETS 3
#define MAX_WORK_GROUP_SIZE 4096

__local uint2 hltables[LOCAL_TABLE_SIZE] __attribute__((xcl_array_partition(cyclic,16,1)));;


kernel
__attribute__ ((reqd_work_group_size(1, 1, 1)))
void calcBucket(__global const uint* keys,
				__global uint* offsets,
				__global uint* BCount,
				__global const uint2* randoms,
				__global ushort* alert,
				const uint buckets,
				const uint bucket_maxsize,
				const uint SIZE,
				const uint attempt)
{
	//const uint gid = get_global_id(0);
	uint bucket_id, key;
	__local uint offset;

 __attribute__((xcl_pipeline_loop))
 for(int i = 0; i<INPUT_SIZE; i++) {
		key = keys[i];

		// Find the bucket id for each key based on the function h(k) = [(c0 + c1*k) % PRIME] % buckets
		bucket_id = ((randoms[attempt].x + randoms[attempt].y * key) % PRIME) % buckets;

		offset = BCount[bucket_id];

		offset++;

		// Check if a bucket has exceeded the maximum size
		if(offset >= bucket_maxsize) {
			*alert = 1;
		}

		if(i == 2) printf("kernel attempt:%d %d %d", bucket_id, BCount[bucket_id], offsets[i] );

		BCount[bucket_id] = offset;

		// Store each key's offset to an array for use in the next kernel
		offsets[i] = offset-1;

		if(i == 2) printf("kernel attempt:%d %d %d", bucket_id, BCount[bucket_id], offsets[i] );
	}
}

kernel
__attribute__ ((reqd_work_group_size(1, 1, 1)))
__kernel void prefixSum(__global const uint* BucketCount,
				__global uint* BucketStart,
				const uint length)
{
	BucketStart[0] = 0;
	 __attribute__((xcl_pipeline_loop))
	for(uint i=1; i<BUCKETS; i++) {
		BucketStart[i] = BucketStart[i-1] + BucketCount[i-1];
	}
}

kernel
__attribute__((reqd_work_group_size(1, 1, 1)))
void shuffleInput(__global const uint* keys,
				__global const uint* offsets,
				__global uint2* Shuffled,
				__global const uint* BStart,
				__constant uint2* rands,
				const uint buckets,
				const uint SIZE)
{
	int i = get_global_id(0);
	uint bucket_id, offset, key;

	key = keys[i];
	bucket_id = ((rands[0].x + rands[0].y * key) % PRIME) % buckets;
	offset = BStart[bucket_id] + offsets[i];
	Shuffled[offset].x = key;
	Shuffled[offset].y = i;

	if(i == 2) printf("kernel attempt:%d %d %d %d",key, Shuffled[offset].x, BStart[bucket_id], offsets[i] );

}

kernel
__attribute__((reqd_work_group_size(MAX_WORK_GROUP_SIZE, 1, 1)))
__kernel void buildTable(__global uint2* outTable,
				__global const uint2* Shuffled,
				__global const uint* BCount,
				__global const uint* BStart,
				__constant ushort2* consts,
				__global ushort* bucket_seeds,
				__constant ushort* g_randoms,
				__global const uint2* deadTable,
				__global ushort* lucky_attempt,
			    __global uint* success,
				const uint local_table_size)
{
	const uint lid = get_local_id(0);
	const uint groupid = get_group_id(0);
	const uint group_size = BCount[groupid];
	const uint group_start = BStart[groupid];
	__local ushort2 newRandoms[SUBTABLES];
	__local bool alert;
	__private bool hashed;
	event_t evnt;
	ushort attempts;
	uint value_in, value_out, key_index, bucket_id, g_index, cur_ofst;


	if (lid < group_size) {
		attempts = lucky_attempt[groupid];
		g_index = group_start + lid;
		value_in = Shuffled[g_index].x;
		key_index = Shuffled[g_index].y;
		//printf("kernel attempt:%d", lucky_attempt[groupid]);
	}

	if (lid < 1) {
		newRandoms[lid] = g_randoms[attempts] ^ consts[lid];
	}
	*success = 1;
	barrier(CLK_LOCAL_MEM_FENCE);

	__attribute__((xcl_pipeline_workitems)) {
		// Initialize the local table with -1
		evnt = async_work_group_copy(hltables, deadTable, LOCAL_TABLE_SIZE, 0);
		wait_group_events(1, &evnt);

		hashed = false;
		if (lid < group_size && !hashed) {
			cur_ofst = 0;
			bucket_id = ((newRandoms[0].x + newRandoms[0].y * value_in) % PRIME)
					% LOCAL_TABLE_SIZE;
			cur_ofst += bucket_id;
			hltables[cur_ofst].x = value_in;
		}
	}


	barrier(CLK_LOCAL_MEM_FENCE); // Synchronize with in work group

	__attribute__((xcl_pipeline_workitems)) {
		if (lid < group_size && !hashed) {
			value_out = hltables[cur_ofst].x;
			if (value_in == value_out) {
				hltables[cur_ofst].y = key_index;
				hashed = true;
			}
		}

		if (lid < group_size && !hashed) {
			*success = 0;
		}
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	if (lid == 0) {
		lucky_attempt[groupid] = attempts;
		bucket_seeds[groupid] = g_randoms[attempts];
	}

	evnt = async_work_group_copy(outTable + (LOCAL_TABLE_SIZE * groupid),
			hltables, LOCAL_TABLE_SIZE, 0);
	wait_group_events(1, &evnt);
}


kernel
__attribute__ ((reqd_work_group_size(4096, 1, 1)))
void queryTable(__global const uint* keys,
				__global uint* indices,
				__global uint2* hashTable,
                __constant uint2* randoms,
                __constant ushort2* constants,
                __global const ushort* seeds,
                const uint local_table_size,
				const uint buckets)
{
	uint index, hash_index;
	ushort2 lrandoms;
	uint2 row;
	int gid = get_global_id(0);
	if(gid > INPUT_SIZE) return;

	//for(int gid = 0; gid< INPUT_SIZE; gid++){

		uint hashkey = keys[gid];

		uint bucket = ((randoms->x + randoms->y * hashkey) % PRIME) % buckets;

		const uint offset = bucket * LOCAL_TABLE_SIZE;

		lrandoms = seeds[0] ^ constants[0];

		hash_index = ((lrandoms.x + lrandoms.y * hashkey) % PRIME) % LOCAL_TABLE_SIZE;

		row = hashTable[offset + hash_index];
		if(row.x == hashkey) {
			indices[gid] = row.y;
		}

		//if(gid == 4094)
			//printf("\n retrieve: %d %d %d %d", get_local_id(0), get_group_id(0), get_local_id(0), get_local_size(0));

	//}
	return;

}
