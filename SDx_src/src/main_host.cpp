#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <random>
#include <limits>
#include <vector>
#include <algorithm>
#include <thread>
#include <stdexcept>
#include <cassert>
#include <iomanip>
#include <CL/cl.h>
#include <cstdlib>
#include "host.h"

using namespace std;
using namespace cl;

//TARGET_DEVICE macro needs to be passed from gcc command line
#if defined(SDX_PLATFORM) && !defined(TARGET_DEVICE)
#define STR_VALUE(arg)      #arg
#define GET_STRING(name) STR_VALUE(name)
#define TARGET_DEVICE GET_STRING(SDX_PLATFORM)
#endif

typedef enum {
	calcBucket, prefixSum, shuffleInput, buildTable, queryTable
} Kernelnames;

const cl_uint subtables = 3;
const cl_uint primenum = 1900813;
const cl_uint max_attempts = 50;
const cl_double buckload = 0.7;
const cl_double gama = 0.8;

static const int DATA_SIZE = 4096;
size_t szLocalWorkSize = 256;
static const std::string error_message = "Error: Result mismatch:\n"
		"i = %d CPU result = %d Device result = %d\n";

int main(int argc, char* argv[]) {

//	try {
		std::string kernelNames[5] = { "calcBucket", "prefixSum", "shuffleInput",
				"buildTable", "queryTable" };

		cl::vector<cl::Platform> platforms;

		Event event;
		cl_uint max_work_group_size;
		NDRange global;
		NDRange local;

		//TARGET_DEVICE macro needs to be passed from gcc command line
		const char *target_device_name = TARGET_DEVICE;

		if (argc != 2) {
			std::cout << "Usage: " << argv[0] << " <xclbin>" << std::endl;
			return EXIT_FAILURE;
		}

		char* xclbinFilename = argv[1];

		std::vector<cl::Device> devices;
		cl::Device device;
		//std::vector<cl::Platform> platforms;
		bool found_device = false;

		//traversing all Platforms To find Xilinx Platform and targeted
		//Device in Xilinx Platform
		cl::Platform::get(&platforms);
		for (size_t i = 0; (i < platforms.size()) & (found_device == false);
				i++) {
			cl::Platform platform = platforms[i];
			std::string platformName = platform.getInfo<CL_PLATFORM_NAME>();
			if (platformName == "Xilinx") {
				devices.clear();
				platform.getDevices(CL_DEVICE_TYPE_ACCELERATOR, &devices);

				//Traversing All Devices of Xilinx Platform
				for (size_t j = 0; j < devices.size(); j++) {
					device = devices[j];
					std::string deviceName = device.getInfo<CL_DEVICE_NAME>();
					if (deviceName == target_device_name) {
						found_device = true;
						break;
					}
				}
			}
		}
		if (found_device == false) {
			std::cout << "Error: Unable to find Target Device "
					<< target_device_name << std::endl;
			return EXIT_FAILURE;
		}

		// Creating Context and Command Queue for selected device
		cl::Context context(device);
		cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE);
		max_work_group_size = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();

		// Load xclbin
		std::cout << "Loading: '" << xclbinFilename << "'\n";
		std::ifstream bin_file(xclbinFilename, std::ifstream::binary);
		bin_file.seekg(0, bin_file.end);
		unsigned nb = bin_file.tellg();
		bin_file.seekg(0, bin_file.beg);
		char *buf = new char[nb];
		bin_file.read(buf, nb);

		std::cout << "capacity " << std::endl;

		// Creating Program from Binary File
		Program::Binaries bins;
		bins.push_back( { buf, nb });
		devices.resize(1);
		cl::Program program(context, devices, bins);
		//////////////////////////////////////////////////////////////////////////////////////////////

		const cl_uint input_size = (1 << 13);
		cl_uint data[input_size];
		for (cl_uint i = 0; i < input_size; i++) {
			data[i] = i;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		//calc buckets

		cl_uint block_load = static_cast<cl_uint>(max_work_group_size * buckload); // Each bucket gets a percentage of the workgroup size
		cl_uint buckets = ((float)input_size) / (float)block_load + (input_size % block_load ? 1 : 0); // Compute the number of buckets and round up
		cl_ushort alert;

		std::cout <<"buckets: "<<buckets<<"  block_load: "<<block_load<<" input_size "<<input_size <<" max_work_group "<<max_work_group_size<<std::endl;

		Buffer d_UniqKeys = Buffer(context, CL_MEM_READ_WRITE,
				input_size * sizeof(cl_uint));
		Buffer d_Indices = Buffer(context, CL_MEM_READ_WRITE,
				input_size * sizeof(cl_uint));
		Buffer d_Shuffled = Buffer(context, CL_MEM_READ_WRITE,
				(2 * input_size) * sizeof(cl_uint));
		Buffer d_BucketCount = Buffer(context, CL_MEM_READ_WRITE,
				buckets * sizeof(cl_uint));
		Buffer d_BucketStart = Buffer(context, CL_MEM_READ_WRITE,
				buckets * sizeof(cl_uint));
		Buffer d_randoms = Buffer(context, CL_MEM_READ_WRITE,
				(2 * max_attempts) * sizeof(cl_uint));
		Buffer d_lucky_randoms = Buffer(context, CL_MEM_READ_WRITE,
				2 * sizeof(cl_uint));
		Buffer d_alert = Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_ushort));
		Buffer d_offsets = Buffer(context, CL_MEM_READ_WRITE,
				input_size * sizeof(cl_uint));

		cl_uint attempt = 0;
		cl_uint c[2 * max_attempts];

		c[0] = 0;
		c[1] = 1;
		for (cl_uint i = 2; i < 2 * max_attempts; i++) {
			c[i] = rand();
		}
		queue.enqueueWriteBuffer(d_randoms, CL_TRUE, 0,
				(2 * max_attempts) * sizeof(cl_uint), c);

		uint* bucks = new uint[buckets];
		for (uint i = 0; i < buckets; i++) {
			bucks[i] = 0;
		}

		queue.enqueueWriteBuffer(d_UniqKeys, CL_TRUE, 0,
				input_size * sizeof(cl_uint), data);

		cl::Kernel kernel(program, kernelNames[calcBucket].c_str());
		kernel.setArg(0, d_UniqKeys);
		kernel.setArg(1, d_offsets);
		kernel.setArg(2, d_BucketCount);
		kernel.setArg(3, d_randoms);
		kernel.setArg(4, d_alert);
		kernel.setArg(5, buckets);
		kernel.setArg(6, max_work_group_size);
		kernel.setArg(7, input_size);

//		global = NDRange(buckets * max_work_group_size);
//		local = NullRange;

		for (attempt = 0; attempt < max_attempts; attempt++) {
			kernel.setArg(8, attempt);
			queue.enqueueWriteBuffer(d_BucketCount, CL_TRUE, 0, buckets * sizeof(cl_uint), bucks); // Initialize BucketCount with 0's
			alert = 0;
			queue.enqueueWriteBuffer(d_alert, CL_TRUE, 0, sizeof(cl_ushort), &alert);

			//Launch the Kernel
			queue.enqueueTask(kernel);

			queue.enqueueReadBuffer(d_alert, CL_TRUE, 0, sizeof(cl_ushort), &alert);

			if (alert == 0) {
				break;
			}
		}


		if (attempt == max_attempts) {
			std::cout << "Failed at level 1" << std::endl;
			return 1;
		} else {
			std::cout << "Build Level 1 ok. attempt: " << attempt<< std::endl;

			 cl_uint* counts = new cl_uint[buckets];
			 queue.enqueueReadBuffer(d_BucketCount, CL_TRUE, 0, buckets * sizeof(cl_uint), counts);
			 std::cout << "Group count: " << (counts[0] )<<" "<< counts[1]<<" "<<counts[2]<<std::endl;
		}
		delete[] bucks;

		std::cout << "TEST " << std::endl;

		queue.enqueueWriteBuffer(d_lucky_randoms, CL_TRUE, 0, 2 * sizeof(cl_uint), c+attempt);

		/////////////////////////////////////////////////////////////////////////////////////////////
		// Prefix Sum on BCount

		kernel = Kernel(program, kernelNames[prefixSum].c_str());
		kernel.setArg(0, d_BucketCount);
		kernel.setArg(1, d_BucketStart);
		kernel.setArg(2, buckets);
		queue.enqueueTask(kernel);

		 cl_uint* starts = new cl_uint[buckets];
		 queue.enqueueReadBuffer(d_BucketStart, CL_TRUE, 0, buckets * sizeof(cl_uint), starts);
		 std::cout << "Group start: " << (starts[0] )<<" "<< starts[1]<<" "<<starts[2]<<std::endl;

		std::cout << "TEST2 " << std::endl;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // Shuffle Data
        kernel = Kernel(program, kernelNames[shuffleInput].c_str());
        kernel.setArg(0, d_UniqKeys);
        kernel.setArg(1, d_offsets);
        kernel.setArg(2, d_Shuffled);
        kernel.setArg(3, d_BucketStart);
        kernel.setArg(4, d_lucky_randoms);
        kernel.setArg(5, buckets);
        kernel.setArg(6, input_size);

        global = NDRange(input_size);
        local = NDRange(1);
        queue.enqueueNDRangeKernel(kernel, NullRange, global, local, NULL, &event);
        //queue.enqueueTask(kernel);
        event.wait();

        ofstream myfile;
        myfile.open ("example.txt");

        cl_uint* sh = new cl_uint[2*input_size];
        queue.enqueueReadBuffer(d_Shuffled, CL_TRUE, 0, 2*input_size*sizeof(cl_uint), sh);
        for(cl_uint i=0; i<input_size; i++)
          {
            myfile << i<<" "<<sh[2*i]<< " "<<sh[2*i+1]<<"\n";
            if(sh[2*i] == 4094)
              {
                std::cout << "shuffled..." << i<<std::endl;
                //return 1;
              }
          }

        myfile.close();
        delete[] sh;
		std::cout << "TEST3 " << std::endl;

	 cl_uint local_table_size = static_cast<cl_uint>(max_work_group_size * (1+gama));

	  std::cout << "\nlocal_table_size: " << local_table_size << std::endl;

	 Buffer d_hashTable = Buffer(context ,CL_MEM_READ_WRITE, (2 * buckets * local_table_size) * sizeof(cl_uint));
	 Buffer d_constants = Buffer(context, CL_MEM_READ_ONLY, (subtables*2) * sizeof(cl_ushort));
	 Buffer d_DeadTable = Buffer(context, CL_MEM_READ_ONLY, (local_table_size * 2) * sizeof(cl_uint));
	 d_randoms = Buffer(context, CL_MEM_READ_WRITE, max_attempts*sizeof(cl_uint));
	 Buffer d_seeds = Buffer(context, CL_MEM_READ_WRITE, buckets*sizeof(cl_ushort));
	 Buffer d_lucky_attempt = Buffer(context, CL_MEM_READ_WRITE, buckets*sizeof(cl_ushort));
	 Buffer d_success = Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_uint));


	 cl_ushort constants[subtables*2];
	 cl_ushort seeds[max_attempts];
	 for(cl_uint i=0; i<subtables*2; i++) {
		 constants[i] = rand() % CL_USHRT_MAX;
	 }
	 for(cl_uint i=0; i<max_attempts; i++) {
		 seeds[i] = rand() % CL_USHRT_MAX;
	 }
	 cl_uint* deadTable = new cl_uint[local_table_size*2];
	 for(cl_uint i=0; i<local_table_size*2; i++) {
		 deadTable[i] = CL_UINT_MAX;
	 }

	 queue.enqueueWriteBuffer(d_constants, CL_TRUE, 0, (subtables*2) * sizeof(cl_ushort), constants);
	 queue.enqueueWriteBuffer(d_randoms, CL_TRUE, 0, (max_attempts) * sizeof(cl_uint), seeds);
	 queue.enqueueWriteBuffer(d_DeadTable, CL_TRUE, 0, (local_table_size * 2) * sizeof(cl_uint), deadTable);


	 for(cl_uint att=0; att<50; att++) {
		 cl_ushort* attempts = new cl_ushort[buckets];
		 for(cl_ushort i=0; i<buckets; i++){
			 attempts[i] = att;
		 }
		 queue.enqueueWriteBuffer(d_lucky_attempt, CL_TRUE, 0, buckets*sizeof(cl_ushort), attempts);

		 kernel = Kernel(program, kernelNames[buildTable].c_str());
		 kernel.setArg(0, d_hashTable);
		 kernel.setArg(1, d_Shuffled);
		 kernel.setArg(2, d_BucketCount);
		 kernel.setArg(3, d_BucketStart);
		 kernel.setArg(4, d_constants);
		 kernel.setArg(5, d_seeds);
		 kernel.setArg(6, d_randoms);
		 kernel.setArg(7, d_DeadTable);
		 kernel.setArg(8, d_lucky_attempt);
		 kernel.setArg(9, d_success);
		 kernel.setArg(10, local_table_size);

	     global = NDRange(buckets*max_work_group_size);
	     local = NDRange(max_work_group_size);//NullRange;
	     queue.enqueueNDRangeKernel(kernel, NullRange, global, local, NULL, &event);
	     event.wait();

		 cl_uint* success = new cl_uint();
		 queue.enqueueReadBuffer(d_success, CL_TRUE, 0, sizeof(cl_uint), success);
		 if(success[0] == 0) { // if it fails, exit
			 std::cout << "failed: attempted: "<<att<<" local_table_size: "<< local_table_size<<std::endl;
			 //delete[] attempts;
		 }else{
			 std::cout << "success: attempted: "<<att<<" local_table_size: "<< local_table_size<<std::endl;
			 break;
		 }
		 delete success;
	 }


	 cl_uint* tt = new cl_uint[(2 * buckets * local_table_size)];
	 queue.enqueueReadBuffer(d_hashTable, CL_TRUE, 0, (2 * buckets * local_table_size) * sizeof(cl_uint), tt);

	//  ofstream myfile;
//	 myfile.open ("example.txt");

	 int count = 0;
	 for(cl_uint i=0; i<2 * buckets * local_table_size; i = i +2) {
//		 myfile << tt[i]<<" "<<tt[i+1]<<"\n";
//		 if(tt[i] == 4094)
//		 {
//			 std::cout << "tt: i:"<<i/2<<" " << tt[i] << " " <<tt[i+1] <<std::endl;
//			 count++;
//		 }
	 }

//	 myfile.close();
	 std::cout << "table: " << count<<" local_table_size: "<< local_table_size<<std::endl;

	 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	kernel = Kernel(program, kernelNames[queryTable].c_str());

	kernel.setArg(0, d_UniqKeys);
	kernel.setArg(1, d_Indices);
	kernel.setArg(2, d_hashTable);
	kernel.setArg(3, d_lucky_randoms);
	kernel.setArg(4, d_constants);
	kernel.setArg(5, d_seeds);
	kernel.setArg(6, local_table_size);
	kernel.setArg(7, buckets);

	global = NDRange(input_size);
	local = NDRange(4096);
	queue.enqueueNDRangeKernel(kernel, NullRange, global, local, NULL, &event);
	event.wait();
	//queue.enqueueTask(kernel);

	 //myfile.open ("example.txt");
	queue.enqueueReadBuffer(d_Indices, CL_TRUE, 0, input_size*sizeof(cl_uint), data);
	for(cl_uint i=0; i<input_size; i++)
	{
		//myfile << i<<" "<<data[i]<<"\n";
		if(data[i] != i)
		{
			std::cout << "Hashing Test failed!" << i<<" "<<data[i]<<std::endl;
			return 1;
		}
	}
	std::cout << "Retrieving indices Test pass!" << std::endl;

	 delete[] deadTable;
//	 delete[] attempts;
	 delete[] tt;
	 return 0;
}
