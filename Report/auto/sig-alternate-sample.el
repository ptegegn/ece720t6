(TeX-add-style-hook
 "sig-alternate-sample"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("algorithm2e" "ruled" "linesnumbered") ("todonotes" "draft")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "latex2e"
    "sig-alternate-05-2015"
    "sig-alternate-05-201510"
    "algorithm2e"
    "listings"
    "lipsum"
    "xcolor"
    "todonotes")
   (LaTeX-add-labels
    "eq:1"
    "eq:2"
    "result1"
    "result2"
    "table")
   (LaTeX-add-bibliographies))
 :latex)

