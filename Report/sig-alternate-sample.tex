\documentclass{sig-alternate-05-2015}
%\usepackage{amsmath}
\usepackage[ruled,linesnumbered]{algorithm2e}
\usepackage{listings}
% \usepackage[noend]{algpseudocode}
% \usepackage{comment}

\usepackage{lipsum}
\usepackage{xcolor}
 \usepackage{lipsum}
\usepackage[draft]{todonotes}
\begin{document}

\title{Hash Table on FPGAs} \subtitle{ECE 720t6}

\numberofauthors{1} 
\author{
  \alignauthor Paulos Tegegn
} \date{30 July 1999}

\maketitle
\begin{abstract}
  This work investigated an efficient parallel method to build a hash table by
  making use of Field Programmable Gate Arrays (FPGAs). It considered other
  empirical parallel hashing algorithms used in a heterogeneous architecture such as
  graphical Processing units (GPUs). Our approach uses two level hashing. The
  empirical experiment results show that for input size of 16K, the parallel hash table lookup time was constant
  irrespective of the number of querying keys while the lookup
  procedure utilized 17\% of the resource available on \textit{Xilinx
    Virtex-7 XC7VX690T-2} device.
   
\end{abstract}

\section{Introduction}

A hash table is a data structure used to implement a dictionary abstract types
that has a constant time lookup. It is made up of two parts: an array of storage
where the actual data to be searched is stored, and a mapping function, also
known as hashing function, that maps the input space to the integer space that
defines the indices of the array.

Hash tables have a significant advantages, especially to large scale problems
wherein the expected running time for most critical operations is required to be
\textbf{O(1)}. In applications with efficient mapping requirement, such as
database indexing, caching and sets, hash tables are the dominant structure
used.

In genomics, the problem of aligning a large set of short DNA sequences to an
already sequenced reference genome is another dominant applications of hash
tables. The task of reconstructing a genome operation, which is given a short
read and a reference genome, to find a position in the reference genome, takes
hours just to do one species genome reconstruction. The critical operations in
most sequencing algorithms has a form of storing and querying a large set of
datas. Hence, speeding up the hash table operations will help in making the DNA
sequence alignment process faster.

As applications grow in size, speeding up the hashing operations on CPUs is
becoming increasing important to keep up with the growth rate. On general
purpose machines, the hash table is stored in memory using the sequential
algorithms for both building the hash table and later retrieving values from it.

%Nowadays, with the advent of multi-core architecture, people have looked at
%parallelizing the operation of both constructing and accessing a hash table.
%However, the parallelization is limited by the number of cores, and the
%computation overhead incurred in cpu executions. The techniques used in
%the serial algorithms to handle conflicts do not map to a highly parallel
%compute architectures such as FPGAs.

On general purpose machines, the table is stored in shared memory using the
sequential algorithms for both building the hash table and later retrieving
values from it. Nowadays, with the advent of multi-core architecture, people
have looked at parallelizing the operation of both constructing and accessing a
hash table. However, the parallelization is limited by the number of cores, and
the computation overhead incurred during cpu executions. Furthermore,
The techniques used in the serial algorithms to handle conflicts do not map to a
highly parallel compute architectures such as FPGAs.

While constructing hash table with common sequential methods such as probing
have expected constant retrieval time, the lookup time for some item in the
 table is not constant. The work of \text{Alcantara \textit{et al.},
 [2009]\cite{Alcantara:2009:RPH:1618452.1618500}} focused on a parallel,
GPU-friendly hash table design that allows construction as well as lookup at
interactive rates. Their strategy used a hybrid method of cuckoo hashing method,
and FKS hashing perfect scheme. The novelty in their approach is the construction
and access times, in practice, scale linearly with the number of elements.

In this work, we focused on the problem of storing a large set of key-value in
parallelized data structure on FPGAs that allows parallel access of multiple random keys in constant time.
This implementation can be applied to problems where the data set is already
available and required to execute randomized lookup operations.

\section{Background and our approach}

\subsection{Parallel Cuckoo Hashing on GPU}

A Cuckoo hash [Pagh and Rodler 2004] \cite{Garcia:2011:CPH:2070781.2024195}
reserves two or more different tables of identical size, each accessed through
different hash functions. \text{Alcantara \textit{et al.},
  [2009]\cite{Alcantara:2009:RPH:1618452.1618500}} used three tables wherein
keys are inserted to the first table if the the slot is not taken, otherwise if
collision occurs it evicts the already inserted keys. The evicted keys are in
turn inserted to the second table, then from the second to third, and from third
back to first. The process loops around until all the keys are inserted or it
reaches a maximum iteration number that leads to construction failure. When ever
the hash table construction fails, the process is re executed with another seed
of hash functions for each table. Upon failure the process is restarted with
different hash functions. Unfortunately, given a number of tables the failure
rate abruptly increases above a certain load factor ratio--the number of keys
that can be inserted successfully over the total hash table size. furthermore,
increasing the number of table at lower load factor would be wasting the
storages. In contrast. our scheme investigates design space by using one large
table size.

The parallel cuckoo hash construction uses a small but very fast shared memory.
The construction has two levels. First, it starts by randomly distributing the
key-values into equally sized buckets by using first level hashing function. If
the allocated maximum bucket size is reached, then the distribution process is
restarted again. This puts limitation the load factor ratio; higher load factors
leads larger failure rate of key distribution. The second step is to
independently do cuckoo hash on those keys that map into the same buckets using
independent hash functions. \text{Alcantara \textit{et al.},
  [2011]\cite{Alcantara201239}} builds the the second level hash with a single
iteration with the help of the atomic operations on NVidia devices. However, it
should be noted that the atomic operation slows the single iteration operation
as compared to the approach that does not use atomic synchronizations.

We realized the implementation on Xilinx FPGAs with OpenCL development
tool. OpenCL is a framework standard for writing programs on heterogeneous systems. As
shown in the figure \ref{opencl}, the host is responsible for the general bookkeeping and
launching of the OpenCL kernels. The device is the element in the
system on which the kernels are executed. the accelerator device is further
divided into a number of compute units depending on the hardware. A compute unit
is also further defined as the elements onto which a work group of a kernel is
executed. while CPU and GPU devices have fixed hardware attributes where the
programmer has control over what the device structure looks like, FPGAs allows
the device to be programmable. \cite{sdx:1}

\begin{figure}
  \centering \includegraphics[height=6cm, width=6cm]{opencl}
  \caption{OpenCL device architecture.}
  \label{opencl}
\end{figure}

\subsection{Our Approach: Parallel Two Level Hashing on FPGAs}

Our work follows the same approach of the two level hashing to build the hash
table. The first level shuffles the input key-values so that all of them fits in
buckets. Since there is no global synchronizations mechanism in FPGAs, the
shuffling task is performed some what in sequential order with in a work item.
However, we used OpenCL optimization techniques such as pipelining to improve
the performance. in this stage, all keys are hashed using the first level
hashing function, and the number of keys mapped onto each buckets is recorded.
During the process if the number of keys in any of the buckets passes the
maximum allocated size, then the table is destroyed and the shuffling stage is
restarted again. In the second stage, the Shuffled input key-values are hashed
onto a table per bucket. The second hashing task is performed inside workgroup.
Each workgroup per bucket initializes a table in a local memory. Each work item
per key compute the second level hash function to find the slot in the local
memory table where the key can be stored. Then all the threads per work group
are synchronized until all the work item reaches the same point in the
execution time by making use of OpenCL \textit{Barrier} construct. Afterwards, each
work items reads their slot again and checks if the inserted key values still
remains in the local table. If the key is overwritten by other thread then the
second level hashing procedure will be restarted again.

The first and second level hashing functions have the form as shown on equation
\eqref{eq:1} and \eqref{eq:2} respectively.

\begin{equation}
  $$\textit{h\textsubscript{1}(k)} = ((a + b * key) mod p) mod |buckets|, where a, b are constants.$$
  \label{eq:1}
\end{equation}

\begin{equation}
  $$\textit{h\textsubscript{2}(k)} = ((c + d * key) mod p) mod |local table size|, where c,d are constants.$$
  \label{eq:2}
\end{equation}

\begin{algorithm}
  \SetAlgoLined Given the input key-value size compute the number of buckets\;
  \ForEach{k in Keys}{ Compute \textit{h\textsubscript{1}(k)} to find the bucket b\textsubscript{k}\; Increment the
    number of keys mapped onto the bucket b\textsubscript{k}\; } Compute prefix Sum to find the
  starting address of each bucket\; Reserve contiguous storage for each bucket\;
  Move the key-values to the new shuffled storage\;
  \caption{(Stage 1) Shuffling the input key-values}
\end{algorithm}

\begin{algorithm}
  \SetAlgoLined Generate random hash function constants for each work group (host
  side)\; \ForEach{Buckets in parallel}{ \ForEach{key in Parallel}{ Compute the
      hash \textit{h\textsubscript{2}(k)} to find the slot where the key maps into\; Write the
      key-value into the slot\; synchronize using barrier per workgroup\; Read
      the key from the same slot\; \If{the key still does not exit}{ restart the
        stage 2\; } } }

  \caption{(stage 2) Hashing the shuffled key-values inside workgroup}
\end{algorithm}

\section{Result}
In this section, We measure the performance of our hash table design over
different number of keys on a FPGAs \textit{Xilinx Virtex-7 XC7VX690T-2} \cite{xilinx:7}. This
device features two independent channels of DDR3 memory capable of 1600MT/s
(fitted with two 8GB SODIMMs), high speed I/O, SATA connections, dual SFP+ ports
supporting 10G Ethernet. The experiments are run on both the sdx CPU emulation as well as hardware emulation.

The two level hashing realization has five OpenCL
kernels: \textit{CalculateBucket, PrefixSum, ShuffleInput, Buildtable, and
  QueryTable.} \textit{CalculateBucket, PrefixSum, and ShuffleInput} are the
kernels used to do the first level hashing while \textit{BuildTable} constructs
the local table within a workgroup in the second level hashing. The last kernel
\textit{QueryTable} is used to do the lookup. As it can be seen in figure
\ref{result1}, \textit{CalculateBucket, PrefixSum,and Buildtable} takes the
lowest time and remains steady as the number of keys increases. However,
\textit{ShuffleInput} takes most of the construction time and increases
linearly as the number of input keys increases. Hence rearranging the input key
value is the expensive process. On the other side, Accessing all the inserted keys only takes a
constant time because \textit{QueryTable} constructed in parallel fashion, and
it scales well as with the number of querying keys.

\begin{figure}
  \centering \includegraphics[height=6cm, width=8cm]{result1}
  \caption{Execution time for each kernel}
  \label{result1}
\end{figure}

Figure \ref{result2} shows the total execution time profile of the two level
hash table construction and retrieval. The trend shows that the total time scales
linearly with the number of keys. This is because the effect of shuffling
dominates through out the execution. However, in the application domain such as
DNA sequence alignment wherein the construction is performed once, and the look
up tasks dominates, this cost will only be incurred at initial stage.

\begin{figure}
  \centering \includegraphics[height=6cm, width=8cm]{result2}
  \caption{Total execution time of building and look up}
  \label{result2}
\end{figure}

The device resource utilization of the kernels depicted on table \ref{table} for 10K
input key-values is very low. The BRAM and DSP usages are less than 1\% the
available device while LUT usage is little over 10\%, which is comparably large.
\textit{BuildTable} takes the most resource areas as it is designed to use the
area in return to get better performance.
\begin{table}
  \begin{center}
    \begin{tabular}{| c| c| c| c| c| }
      \hline
      Module Name & FF &  LUT & DSP & BRAM\\
      \hline
      buildTable  & 	12951&	14828 &	8 &	60\\
      calcBucket  &	17409&	16027 &	8 &	4\\
      prefixSum   &	1676 &	2333  & 0 &	2\\
      shuffleInput&	5671 &	7243  &	4 &	4\\
      queryTable  & 	6062 &	7022  &	12&	4\\
      \hline
      Total      &  43769 &	47453	& 32&	74\\
      \hline
      Device Resource & 866,400 &	433,200&	3,600&	52,920\\
      \hline
      Resource Utilization & 5.1	&11	&0.9	&0.1\\
      \hline
    \end{tabular}
  \end{center}
  \caption{Area utilization of the kernels on \textit{Xilinx Virtex-7
      XC7VX690T-2}}
  \label{table}
\end{table}


\section{Conclusions}

We have looked at a simple parallel hash table implementation on FPGAs that
give us constant time for multiple parallel key lookup. It follows similar
techniques used to realized parallel hash table on graphical processing units
(GPUs). We saw a constant look up time irrespective of the input key size while
with comparable construction time. for input key size of 16K, constant lookup was 
achieved independent of the number of querying keys while utilizing 17\% of the resource available on device

\subsection{Further work}

Figure \ref{result1} shows that \textit{shufflingInput} is the most expensive
task of all the kernels. This is because it is using a sequential algorithm of
rearrange the inputs into a group of buckets. The reason why sequential was
chosen because there is no global synchronization method in Xilinx OpenCL
framework. This points out that there is a work left to improve the performance rearranging kernel.    

\bibliographystyle{unsrt} \bibliography{sig-alternate-sample}

\end{document}
