# Hash Table on FPGAs

### SDx_src contains the the source code for hashtable project.

### Report is the dir where the report latex ssource is located.

### How to run this project on SDx:
*  Launching SDx.
*  In the SDx Eclipse GUI, File → Import menu command and choose `SDx_src/project.sdx`
*  Then Run 

Note, you can choose Emulation-CPU, Emulation-HW or System in `project.sdx`